
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
    //  m = (x + y) / 2.0;
      m = berrechneMittelwert (x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double berrechneMittelwert( double x1,double y2) {
	   double mittel;
	   mittel = (x1 + y2) /2;
	   return mittel;
   }
}
