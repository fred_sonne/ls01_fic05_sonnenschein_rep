﻿import java.util.Scanner;


//fahrkartenbestellungErfassen
//fahrkartenBezahlen
//fahrkartenAusgeben
//rueckgeldAusgeben



class Fahrkartenautomat
{

	
    public static void main(String[] args)
    {
    	
    	// Bespiel Nachkommastellen
    	double test = 2;
    	System.out.printf(" Bsp: Nachkommastellen. Zu zahlen: %.2f Euro \n" , test );
    	System.out.printf(" Notieren Sie die Variablen, die im Quelltext deklariert werden, und die Datentypen dieser Variablen.\n" + 
    			"Welche Operationen werden mit den Variablen ausgeführt?  \n Habe ich als Kommentar in den Code geschrieben.\n\n\n" );
    	
    	
    	
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       boolean bool = true;

       
       System.out.print("Zu zahlender Betrag (EURO): "); //(double zuZahlenderBetrag eingabe)
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets: "); //(double zuZahlenderBetrag eingabe, Multiplikation)
       zuZahlenderBetrag = zuZahlenderBetrag * tastatur.nextDouble(); 
       //Begründen Sie Ihre Entscheidung für die Wahl des Datentyp: duoble kann Brüche ausgeben (halbe Euros)
       //Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert:
       //der neue zuZahlenBetrag wird vervielfälltigt, der Anzahl der Tickets linear entsprechend.
      
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro " , (zuZahlenderBetrag  - eingezahlterGesamtbetrag )); // (double zuZahlenderBetrag-eingezahlterGesamtbetrag subtraktion)
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): "); //(double eingeworfeneMünze eingabe)
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;  
       }
       
       
       
       
       
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.00)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " , rückgabebetrag ); //(double rückgabebetrag ausgabe)
    	   System.out.println("wird in folgenden Münzen ausgezahlt:"); //(double rückgabebetrag Bedingung/Vergleich)

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}