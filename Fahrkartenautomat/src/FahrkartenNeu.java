import java.util.Scanner;


class FahrkartenNeu { 
static Scanner tastatur = new Scanner(System.in);

   public static void warte(int millisekunde)
  {
  
                   try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

  
  public static void main(String[] args)  {
    
        while (true) {
            double zuZahlenderBetrag = fahrkartenbestellungErfassen();
            double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);


            fahrkartenAusgeben();

            rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
        }

  }
    
    public static double fahrkartenbestellungErfassen() {
      
      double anzahl = 0;
      double zuZahlenderBetrag = 0;
       

    
       System.out.print("Zu zahlender Betrag (EURO): "); 
           zuZahlenderBetrag = tastatur.nextDouble();
         System.out.print("Anzahl der Tickets: ");
           anzahl =  tastatur.nextDouble();
           zuZahlenderBetrag *= anzahl;
           return zuZahlenderBetrag;
    }
    
      public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneM�nze = 0;
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
          {
           System.out.printf("Noch zu zahlen: %.2f Euro " , (zuZahlenderBetrag  - eingezahlterGesamtbetrag )); 
           System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
           eingeworfeneM�nze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneM�nze;  
          }
        return eingezahlterGesamtbetrag;
      }
      
          public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(50);
        }
        System.out.println("\n\n");
    }

             public static void muenzeAusgeben(int betrag){
    
    //betrag * 100 wegen int   200 = 2 euro
                         while (betrag > 0 ) {
                               if   (betrag>= 200)
                                    {
                                     System.out.println("2 EURO");betrag = betrag-200;
                                      }
                                if   (betrag == 100)
                                    {
                                     System.out.println("1 EURO");betrag = betrag-100;
                                     }           
                                  if   ((betrag>= 50)&&(betrag < 100)  )
                                    {
                                     System.out.println("50 CENT");betrag = betrag-50;
                                      }
                                     if   ((betrag>= 20)&&(betrag < 50))
                                    {
                                     System.out.println("20 CENT");betrag = betrag-20;
                                      }
                                       if   ((betrag>= 10)&&(betrag < 20)  )
                                    {
                                     System.out.println("10 CENT");betrag = betrag-10;
                                      }
                                       if   ((betrag>= 5 )&&(betrag < 10) )
                                    {
                                     System.out.println("5 CENT");betrag = betrag-5;
                                      }
      
                                     }  }
                        
        
      public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) { 
        double r�ckgabebetrag;
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
          if(r�ckgabebetrag > 0.00)
          {
           System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro " , r�ckgabebetrag ); 
           System.out.println("wird in folgenden M�nzen ausgezahlt:"); 
      
                r�ckgabebetrag = r�ckgabebetrag * 100;
                 int value = (int)r�ckgabebetrag;
                muenzeAusgeben(value);

              
          }

          System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                             "vor Fahrtantritt entwerten zu lassen!\n"+
                             "Wir w�nschen Ihnen eine gute Fahrt.");
      }


}


